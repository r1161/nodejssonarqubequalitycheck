import moment from "moment";
import * as httpStatusCode from "http-status-codes";
import { respondError, respondSuccess } from "../helpers/responseHelper";
import {
  getPaymentDetailsForPayment,
  getPaymentPageDetailsForPayment,
  getPaymentSuccessDetails,
  getQuoteForDGI,
  getTransactionListFromFinpower,
  makePaymentForTill,
  makePaymentToFinPower,
  makeStaticPaymentForTill,
  markPaymentExpired,
  sendConfirmationToSpecificNumberAndEmail,
  sendDeviceVerificationCode,
  verifyDevice,
  getVerificationCodeDetails,
  getQuotationData,
  updateIdMatrixVerification,
  getIdMatrixVerification,
  checkEnquiryIdExists,
  removePayLater,
  saveEmailAndPhoneToPR,
  sendFailedConfirmationToSpecificNumberAndEmail, updatePhoneInPR,
} from "../repositories/paymentRepository";
import Lang from "../constants/constants";
import { idMatrixVerify } from "../services/idMatrixVerificationService";
import { verifyCustomerEmailUsingToken } from "../repositories/customerRepository";
import { addIdentity } from "../repositories/paymentRequestRepository";
import { getCommunicationById } from "../repositories/communicationRepository";
import {
  sendCommunication,
  sendReturnCallBack,
} from "../helpers/genericFunction";
import { getPartnerById } from "../repositories/partnerRepository";

export const getPaymentDetails = async (req, res) => {
  let paymentDetails = null;
  const { transactionId } = req.query;
  try {
    paymentDetails = await getPaymentDetailsForPayment(transactionId);
    // display payment cancelled message
    if (paymentDetails.paymentDetails.paymentStatus === "Cancelled") {
      return respondError(
        res,
        httpStatusCode.BAD_REQUEST,
        Lang.PAYMENT_REQUEST_CANCELLED
      );
    }

    // display payment completed message
    if (paymentDetails.paymentDetails.paymentStatus === "Completed") {
      return respondError(
        res,
        httpStatusCode.BAD_REQUEST,
        Lang.PAYMENT_REQUEST_COMPLETED
      );
    }

    if (paymentDetails.paymentDetails.paymentStatus === "Active") {
      return respondError(
        res,
        httpStatusCode.BAD_REQUEST,
        Lang.PAYMENT_REQUEST_COMPLETED
      );
    }
    // check if expired and if true display expired error
    const isExpired = moment(Date.now()).isAfter(
      new Date(paymentDetails.expiredDate)
    );

    // if request is expired but status is not expired, mark as expired
    if (isExpired) {
      if (paymentDetails.paymentDetails.paymentStatus !== "Expired") {
        await markPaymentExpired(transactionId);
        const payload ={
          customer: paymentRequestExits.customerId,
          status: 'Expired',
          amount: paymentRequestExits.amount,
          type: 'PR',
          reference: paymentRequestExits.reference,
          requestType:paymentRequestExits.requestType ,
          paymentRequestId: transactionId
        }
        sendReturnCallBack(transactionId);
      }

      // show error

      return respondError(
        res,
        httpStatusCode.BAD_REQUEST,
        Lang.PAYMENT_REQUEST_EXPIRED
      );
    }

    return respondSuccess(
      res,
      200,
      "Payment",
      "Payment Details retrieved successfully.",
      paymentDetails
    );
  } catch (e) {
    console.log("Payment details", e);
  }
};

export const getPaymentPageDetails = async (req, res) => {
  let paymentPageDetails = null;
  const { pageId } = req.query;
  try {
    paymentPageDetails = await getPaymentPageDetailsForPayment(pageId);
    return respondSuccess(
      res,
      200,
      "Payment",
      "Payment Details retrieved successfully.",
      paymentPageDetails
    );
  } catch (e) {
    console.log("Payment details error", e);
  }
};

export const getPaymentPageSuccessDetails = async (req, res) => {
  let paymentDetails = null;
  const { transactionId } = req.query;
  try {
    paymentDetails = await getPaymentSuccessDetails(transactionId);
    return respondSuccess(
      res,
      200,
      "Payment",
      "Payment Details retrieved successfully.",
      paymentDetails
    );
  } catch (e) {
    console.log("Payment details", e);
  }
};

export const makePayment = async (req, res) => {
  let paymentDetails = null;

  const details = await getPaymentDetailsForPayment(req.body.paymentRequestId);
  // console.log(details.paymentStatus)
  if (!['Pending','Re-sent'].includes(details.paymentDetails.paymentStatus) ) {
    console.log(details)
    return respondError(
      res,
      httpStatusCode.BAD_REQUEST,
      "Payment",
      Lang.PAYMENT_REQUEST_CANNOT_BE_MADE
    );
  }

  try {
    if (req.body.payment_mode && req.body.payment_mode === "later") {
      paymentDetails = await makePaymentToFinPower(req.body);
      if (paymentDetails.Imported) {
        return respondSuccess(
          res,
          200,
          "Payment",
          "Payment has been made successfully.",
          paymentDetails
        );
      }
    } else {
      paymentDetails = await makePaymentForTill(req.body);
      console.log("Recurring");
      console.log(paymentDetails);
    }

    if (!paymentDetails) {
      return respondError(
        res,
        500,
        "Payment",
        "Please change your card. We are not able to make the first payment with your card."
      );
    }

    if (paymentDetails.Message) {
      return respondError(
        res,
        400,
        "Payment",
        paymentDetails.Message,
        paymentDetails
      );
    }

    if (paymentDetails.errorCode) {
      return respondError(
        res,
        500,
        "Payment",
        paymentDetails.errorMessage,
        paymentDetails
      );
    }

    if (paymentDetails.success === false) {
      return respondError(
        res,
        500,
        "Payment",
        paymentDetails.errors
          ? paymentDetails.errors[0].errorMessage
          : paymentDetails.errorMessage,
        paymentDetails
      );
    }

    await addIdentity(req.body.mobile);
    sendReturnCallBack(req.body.paymentRequestId);
    return respondSuccess(
      res,
      200,
      "Payment",
      "Payment has been made successfully.",
      paymentDetails
    );
  } catch (e) {
    console.log("Payment details", e);
    return respondError(
      res,
      500,
      "Payment",
      "Payment cannot be made. please try again"
    );
  }
};

export const makeStaticPayment = async (req, res) => {
  let paymentDetails = null;
  try {
    paymentDetails = await makeStaticPaymentForTill(req.body);
    if (paymentDetails.errorCode) {
      return respondError(res, 500, "Payment", paymentDetails.errorMessage);
    }
    return respondSuccess(
      res,
      200,
      "Payment",
      "Payment has been made successfully.",
      paymentDetails
    );
  } catch (e) {
    console.log("Payment details", e);
    return respondError(
      res,
      500,
      "Payment",
      "Payment cannot be made. please try again"
    );
  }
};

export const getQuoteFromFinPower = async (req, res) => {
  try {
    const response = await getQuoteForDGI(req.body);
    return respondSuccess(
      res,
      200,
      "Quote",
      "Quote retrieved successfully from finpower",
      response
    );
  } catch (e) {
    console.log("Error", e);
    return respondError(
      res,
      500,
      "Payment",
      "Quote cannot be retrieved at the moment. Please try again"
    );
  }
};

export const getQuotationForPayLaterPlan = async (req, res) => {
  try {
    const response = await getQuotationData(req.body);
    return respondSuccess(
      res,
      200,
      "Quote",
      "Quote retrieved successfully from finpower",
      response
    );
  } catch (e) {
    console.log("Error", e);
    return respondError(
      res,
      500,
      "Payment",
      "Quote cannot be retrieved at the moment. Please try again"
    );
  }
};

export const sendTestEmail = async (req, res) => {
  try {
    /* console.log("Hello wlrd");
    let paymentRequestUpcomingId = "60f14d1f3f0aea6afc2c645b";
    let paymentRequestFailedId = '60f14d543f0aea6afc2c6469';
    let paymentRequestSuccessId = '60f14d543f0aea6afc2c6469';

    let communicationDetails = await getCommunicationById(paymentRequestSuccessId);
    let details = await sendCommunication(communicationDetails);*/
    /*const response = await convertJSONToXML({
      test: '1234'
    });*/
    /*const response = await createCustomerForTest();
    let customerDetails = await findCustomerById(response._id);
    customerDetails.emailVerifyToken = UUID.generate();
    await customerDetails.save();

    let result = await sendCustomerVerificationMail(customerDetails);*/

    /*const details = await getCustomerDetails(response._id);

    const mailStatus = await sendCustomerCreateMail(response, details.partnerId);*/

    let customerDetails = {};
    let details = {};
    let paymentRequestId = "610a522e615e65883e4602df";
    let result = await sendReturnCallBack(paymentRequestId);
    return respondSuccess(
      res,
      200,
      "Quote",
      "Quote retrieved successfully from finpower",
      result
    );
  } catch (e) {
    console.log("Error", e);
    return respondError(
      res,
      500,
      "Payment",
      "Quote cannot be retrieved at the moment. Please try again"
    );
  }
};

export const sendConfirmationForTransaction = async (req, res) => {
  try {
    console.log("req.body",req.body);
    
    const { paymentRequestId } = req.body;
    const { email } = req.body;
    const { mobile } = req.body;
    await saveEmailAndPhoneToPR(paymentRequestId, email, mobile);
    const response = await sendConfirmationToSpecificNumberAndEmail(
      paymentRequestId,
      email,
      mobile
    );
    return respondSuccess(
      res,
      200,
      "Quote",
      "Confirmation has been sent successfully to the user.",
      response
    );
  } catch (e) {
    console.log("Error", e);
    return respondError(
      res,
      500,
      "Payment",
      "Confirmation cannot be sent at the moment. Please try again"
    );
  }
};

export const sendFailedConfirmationForTransaction = async (req, res) => {
  try {
    const { paymentRequestId } = req.body;
    const { email } = req.body;
    const { mobile } = req.body;
    const { transactionID } = req.body;

    const response = await sendFailedConfirmationToSpecificNumberAndEmail(
      paymentRequestId,
      email,
      mobile,
      transactionID
    );
    return respondSuccess(
      res,
      200,
      "Quote",
      "Confirmation has been sent successfully to the user.",
      response
    );
  } catch (e) {
    console.log("Error", e);
    return respondError(
      res,
      500,
      "Payment",
      "Confirmation cannot be sent at the moment. Please try again"
    );
  }
};

export const sendDeviceVerificationCodeForPaylater = async (req, res) => {
  try {
    const { paymentRequestId } = req.body;
    const { mobile } = req.body;

    const response = await sendDeviceVerificationCode(paymentRequestId, mobile);
    return respondSuccess(
      res,
      200,
      "Verification",
      "Device verification code has been sent successfully to the user.",
      response
    );
  } catch (e) {
    console.log("Error", e);
    return respondError(
      res,
      500,
      "Verification",
      "Device verification code cannot be sent at the moment. Please try again"
    );
  }
};

export const verifyDeviceForPaylater = async (req, res) => {
  try {
    const { paymentRequestId, mobile, verification_code } = req.body;



    let codeDetails = await getVerificationCodeDetails(paymentRequestId);
    if (!codeDetails) {
      return respondError(
        res,
        500,
        "Verification",
        "Device cannot be verified successfully at the moment. Please try sending the device verification code and try again"
      );
    }

    const isExpired = moment(Date.now()).isAfter(
      new Date(codeDetails.expiredDate)
    );
    if (isExpired) {
      return respondError(
        res,
        500,
        "Verification",
        "Device cannot be verified successfully at the moment. Please try sending the device verification code and try again"
      );
    }

    const response = await verifyDevice(
      paymentRequestId,
      mobile,
      verification_code
    );
    if (!response) {
      return respondError(
        res,
        500,
        "Verification",
        "Invalid verification code. Please try sending the device verification code and try again"
      );
    }

    await updatePhoneInPR(paymentRequestId,mobile);

    return respondSuccess(
      res,
      200,
      "Verification",
      "Device has been verified successfully.",
      response
    );
  } catch (e) {
    console.log("Error", e);
    return respondError(
      res,
      500,
      "Verification",
      "Device cannot be verified successfully at the moment. Please try again"
    );
  }
};

export const verifyDetailsIDMatrix = async (req, res) => {
  try {
    const { id } = req.params;
    const {
      enquiryId,
      familyName,
      firstGivenName,
      otherGivenName,
      dateOfBirth,
      gender,
      property,
      unitNumber,
      streetNumber,
      streetName,
      streetType,
      suburb,
      state,
      postCode,
      country,
      license,
      passport,
      consent,
      merchantId,
    } = req.body;

    let data = {
      id,
      enquiryId, // Optional
      familyName,
      firstGivenName,
      otherGivenName, // Optional
      dateOfBirth,
      gender, // Optional
      property, // Optional
      unitNumber, // Optional
      streetNumber,
      streetName,
      streetType,
      suburb,
      state,
      postCode,
      country, // Optional
      license,
      passport,
    };
    const merchant = await getPartnerById(merchantId);
    if (merchant.idMatrixService) {
      const paymentRequest = await getIdMatrixVerification(id);
      // check if already requested 3 times
      if (paymentRequest.idMatrixVerifications.length >= 3) {
        return respondError(
          res,
          500,
          "ID Verification",
          "Verified too many times"
        );
      }

      // check if already requested 1 time and if enquiry id matches
      if (paymentRequest.idMatrixVerifications.length === 1) {
        const enquiryIdExists = await checkEnquiryIdExists(id, enquiryId);
        if (!enquiryId || !enquiryIdExists) {
          return respondError(
            res,
            500,
            "ID Verification",
            "Enquiry Id is wrong"
          );
        }
      }

      const jsObject = await idMatrixVerify(data);
      const responseEnquiryId =
        jsObject["soapenv:Envelope"]["soapenv:Header"][0]["wsa:MessageID"][0];
      const overallOutcome =
        jsObject["soapenv:Envelope"]["soapenv:Body"][0]["ns5:response"][0][
          "ns5:response-outcome"
        ][0]["ns5:overall-outcome"][0];

      const fraudAssessment =
        jsObject["soapenv:Envelope"]["soapenv:Body"][0]["ns5:response"][0][
          "ns5:component-responses"
        ][0]["ns5:fraud-assessment-response"][0];

      const updatedPR = await updateIdMatrixVerification(id, {
        enquiryId: responseEnquiryId,
        overallOutcome,
        fraudAssessment: fraudAssessment,
        consent,
      });

      let response = {
        enquiryId: responseEnquiryId,
        overallOutcome,
        xml: jsObject.idmResponse,
        PR: updatedPR,
      };

      if (
        overallOutcome === "REJECT" &&
        updatedPR.idMatrixVerifications.length >= 3
      ) {
        await removePayLater(id);
        response["payLaterRemoved"] = true;
      }

      return respondSuccess(
        res,
        200,
        "ID Verification",
        "Verification retrieved successfully from ID Matrix",
        response
      );
    } else {
      let response = {
        overallOutcome: "ACCEPT",
      };
      return respondSuccess(
        res,
        200,
        "ID Verification",
        "Verification retrieved successfully from ID Matrix",
        response
      );
    }
  } catch (e) {
    console.log("Error", e);
    return respondError(
      res,
      500,
      "ID Verification",
      "Verification cannot be done at the moment. Please try again"
    );
  }
};

export const verifyCheck = async (req, res, next) => {
  try {
    const {
      enquiryId,
      familyName,
      firstGivenName,
      otherGivenName,
      dateOfBirth,
      gender,
      property,
      unitNumber,
      streetNumber,
      streetName,
      streetType,
      suburb,
      state,
      postCode,
      country,
      license,
      passport,
    } = req.body;

    let data = {
      enquiryId, // Optional
      familyName,
      firstGivenName,
      otherGivenName, // Optional
      dateOfBirth,
      gender, // Optional
      property, // Optional
      unitNumber, // Optional
      streetNumber,
      streetName,
      streetType,
      suburb,
      state,
      postCode,
      country, // Optional
      license,
      passport,
    };

    const jsObject = await idMatrixVerify(data);

    return respondSuccess(
      res,
      200,
      "ID Verification",
      "Verfification retrieved successfully from ID Matrix",
      jsObject.idmResponse
    );
  } catch (e) {
    console.log("Error", e);
    return respondError(
      res,
      500,
      "ID Verification",
      "Verification cannot be done at the moment. Please try again"
    );
  }
};

export const verifyCustomerEmail = async (req, res, next) => {
  try {
    let verification_token = req.body.verificationToken;
    let partnerId = req.body.partnerId;

    let response = await verifyCustomerEmailUsingToken(
      partnerId,
      verification_token
    );
    if (!response) {
      return respondError(
        res,
        500,
        "Email Verification",
        "Verification cannot be done at the moment. Please try again"
      );
    }

    return respondSuccess(
      res,
      200,
      "Verification",
      "Email has been verified successfully. Please login using the portal"
    );
  } catch (e) {
    console.log(e);
    return respondError(
      res,
      500,
      "Verification",
      "Verification cannot be done at the moment. Please try again"
    );
  }
};
