import * as httpStatusCode from "http-status-codes";
import { respondSuccess } from "../helpers/responseHelper";
import Lang from "../constants/constants";
import TokenModel from "../models/token";
import { getAllSuburbs } from "../repositories/suburbRepository";

export const checkTokenExpiry = async (req, res, next) => {
  const { token } = req.body;
  if (!token) {
    const error = new Error("Invalid token");
    error.status = error.code = 422;
    error.title = "Check Token";
    next(error);
  }
  return await TokenModel.findOne({ token, isUsed: false })
    .then((result) => {
      if (result !== null) {
        return respondSuccess(
          res,
          httpStatusCode.OK,
          Lang.TOKEN_IS_VALID,
          result
        );
      }
      const error = new Error("Invalid token");
      error.status = error.code = 422;
      error.title = "Check Token";
      next(error);
    })
    .catch((err) => {
      next(err);
    });
};

// delete car
export const getPostcodes = async (req, res, next) => {
  const { searchText } = req.query;
  const page = req.query.page || 1;
  const limit = req.query.limit || 10;
  await getAllSuburbs({ searchText, page, limit })
    .then((postcodes) =>
      respondSuccess(
        res,
        httpStatusCode.OK,
        Lang.SUBURB_LIST_SUCCESS,
        postcodes
      )
    )
    .catch((e) => {
      const error = new Error(e.message);
      error.status = error.code = 422;
      error.title = "GET SUBURB";
      next(error);
    });
};

export const getRefreshTokenGoogle = async (req, res, next) => {
  return respondSuccess(
    res,
    httpStatusCode.OK,
    Lang.SUBURB_LIST_SUCCESS,
    req.body
  );
};
